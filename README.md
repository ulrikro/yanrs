# YANRS

YANRS (Yet Another News Recommender System). This project implements 4 simple recommender system models for news articles.  


The four implemented recommender system can be found in the `yanrs/recommenders` folder. Usage and the results for each model is also presented in their own jupyter notebook.

| Model Name | Model Implementation | Notebook |
| --- | --- | --- |
| User-based Collaborative Filtering | [user_based_collaborative_filtering_speed.py](./yanrs/recommenders/user_based_collaborative_filtering_speed.py) | [user_based_collaborative_filtering.ipynb](./user_based_collaborative_filtering.ipynb) |
| SVD Recommender | [svd_recommender.py](./yanrs/recommenders/svd_recommender.py) | [svd.ipynb](./svd.ipynb) |
| SGD Recommender | [sgd_recommender.py](./yanrs/recommenders/sgd_recommender.py) | [sgd.ipynb](./sgd.ipynb) |
| Content Based | [content_based_recommender.py](./yanrs/recommenders/content_based_recommender.py) | [content_based_filtering.ipynb](./content_based_filtering.ipynb) |

## Data

To be able to run the notebooks, you need the data in the `./data` folder provided in the [GitLab repository](https://gitlab.stud.idi.ntnu.no/ulrikro/yanrs). 

IF YOU ONLY HAVE THE CODE PROVIDED IN THE HANDED IN ZIP FOLDER, YOU HAVE TO CLONE THE REPOSITORY FROM GITLAB TO GET THE DATA TO RUN THE NOTEBOOKS.

If you want to preprocess the data from scratch, you need the active1000 users adressa dataset as well as the articles dataset. To preprocess each of these datasets, run the main functions in the files in the `yanrs/preprocessing` folder. 



## Development

This project uses Python 3.10 and setuptools.

It is also important to use absolute imports: `from yanrs.module.file import func`

### One time setup
```bash
python3.10 -m venv venv  # create a venv with python 3.10
source venv/bin/activate  # windows: venv/Scripts/activate
pip install .
```

The virtual environment (venv) has to be activated each time you use the repository.
Additionally, for Tf-Idf and word2vec embedding, one has to run: 
```bash
python -m spacy download nb_core_news_md
```

In case one wants to create the bert embeddings, one has to install:
- pytorch
- transformers


### Running notebooks
When running notebooks, make sure the python environment from the virtual environment is used.




