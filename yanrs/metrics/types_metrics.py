from typing import Protocol

import numpy as np


class Metric(Protocol):
    def __call__(self, y_pred: np.ndarray, y_true: np.ndarray) -> float:
        ...
