import numpy as np


def mean_squared_error(
    y_pred: np.ndarray,
    y_true: np.ndarray,
) -> float:
    """Evaluates the prediction by comparing it to the testset and using the given error function.
    But only values that are not NaN in the testset are compared.

    Args:
        prediction (np.ndarray): The matrix of predictions.
        testset (np.ndarray): The matrix of testvalues.
        error_function (Callable[[np.ndarray, np.ndarray], float]): The function to calculate the
        error.

    Returns:
        float: The error of the prediction.
    """
    indices_of_testvalues = ~np.isnan(y_true)
    return np.mean(
        np.square(y_pred[indices_of_testvalues] - y_true[indices_of_testvalues])
    ).item()
