import numpy as np


def pearson(u_arr: np.ndarray, v_arr: np.ndarray) -> float:
    """The pearson correlation coefficient between two vectors u and v."""
    avg_rating_u = np.mean(u_arr[np.isfinite(u_arr)])  # type: ignore
    avg_rating_v = np.mean(v_arr[np.isfinite(v_arr)])  # type: ignore
    common_rating_indices, *_ = np.where(np.isfinite(u_arr) & np.isfinite(v_arr))  # type: ignore
    u_normalized = u_arr[common_rating_indices] - avg_rating_u
    v_normalized = v_arr[common_rating_indices] - avg_rating_v
    top = np.sum(u_normalized * v_normalized)
    bottom_u = np.sqrt(np.sum(np.square(u_normalized)))
    bottom_v = np.sqrt(np.sum([np.square(v_normalized)]))  # type: ignore
    if bottom_u == 0 or bottom_v == 0 or top == 0:
        return 0
    return top / (bottom_u * bottom_v)
