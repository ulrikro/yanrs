import numpy as np
from sklearn.metrics import f1_score  # type: ignore


def calculate_like_dislike_score(
    y_pred: np.ndarray,
    y_true: np.ndarray,
    metric=f1_score,
    like_threshold=0.15824915824915825,
) -> float:
    """Calculates the metric of the predicted ratings compared to the ground truth ratings.
    Only non nan values from the ground truth ratings are compared.
    Args:
        predicted_ratings (np.ndarray): The predicted ratings.
        ground_truth_ratings (np.ndarray): The ground truth ratings.
        metric (Callable[[np.ndarray, np.ndarray], float], optional): The metric to calculate
            the score.
        like_threshold (float, optional): The threshold for the like/dislike matrix.
            The default was chosen by the 50th percentile of the rating matrix.
            np.nanpercentile(m, 50)

    Returns:
        float: The f1 score.
    """
    not_non = ~np.isnan(y_true)
    predicted_ratings_filtered = y_pred[not_non]
    ground_truth_ratings_filtered = y_true[not_non]
    # check if predicted_ratings_filtered contains nan values
    if np.isnan(predicted_ratings_filtered).any():
        raise ValueError(
            "The predicted ratings contain nan values where the ground truth ratings are not nan."
        )
    predicted_like_dislike = np.where(
        predicted_ratings_filtered >= like_threshold, 1, 0
    )
    ground_truth_like_dislike = np.where(
        ground_truth_ratings_filtered >= like_threshold, 1, 0
    )
    return float(metric(ground_truth_like_dislike, predicted_like_dislike))  # type: ignore


def like_dislike_f1_score(
    y_pred: np.ndarray,
    y_true: np.ndarray,
    like_threshold=0.15824915824915825,
) -> float:
    """Calculates the f1 score of the predicted ratings compared to the ground truth ratings.
    Only non nan values from the ground truth ratings are compared.
    Args:
        predicted_ratings (np.ndarray): The predicted ratings.
        ground_truth_ratings (np.ndarray): The ground truth ratings.
        like_threshold (float, optional): The threshold for the like/dislike matrix.
            The default was chosen by the 50th percentile of the rating matrix.
            np.nanpercentile(m, 50)

    Returns:
        float: The f1 score.
    """
    return calculate_like_dislike_score(
        y_pred, y_true, metric=f1_score, like_threshold=like_threshold
    )
