import numpy as np
from scipy.stats import kendalltau  # type: ignore


def kendall(y_pred: np.ndarray, y_true: np.ndarray):
    """Calculates the kendall tau correlation between two arrays
    or two matrices.

    Args:
        y_pred (np.ndarray): The predicted ordered values.
        y_true (np.ndarray): The true ordered values.

    Raises:
        ValueError: If the arrays/matrices are not of the same shape.

    Returns:
        float: The kendall tau correlation.
    """
    if y_pred.shape != y_true.shape:
        raise ValueError(
            "The shapes of the predicted and true values must be the same."
        )
    # A NAIVE implementation of the kendall tau correlation.
    # credit_sum = np.zeros(y_pred.shape)
    # for i in tqdm(range(y_pred.shape[0])):
    #     for j in range(i + 1, y_pred.shape[0]):
    #         p = np.sign(y_pred[i] - y_pred[j])
    #         t = np.sign(y_true[i] - y_true[j])
    #         credit_sum += p * t
    # return np.mean(credit_sum / (y_pred.shape[0] * (y_pred.shape[0] - 1) / 2))

    # A FASTER implementation of kendall, but is not very space efficient.
    # x = y_pred
    # y = y_true

    # n = len(x)
    # # Compute all pairwise differences between elements in x and y
    # dx = np.subtract.outer(x, x)
    # dy = np.subtract.outer(y, y)
    # # Count the number of concordant and discordant pairs
    # concordant = np.sum((dx * dy > 0).astype(int))
    # discordant = np.sum((dx * dy < 0).astype(int))

    # # Compute Kendall's tau
    # tau = (concordant - discordant) / (0.5 * n * (n - 1))
    # return tau

    # Scipy's implementation of kendall tau correlation which have none of these problems
    return kendalltau(y_pred, y_true).correlation


def avg_kendall_per_user_recommendations(
    y_pred: list[np.ndarray], y_true: list[np.ndarray]
) -> float:
    """Calculates the tau correlation coefficient between each array in the list
    and takes the average of all the values.

    Args:
        y_pred (list[np.ndarray]): The list of predicted ordered values.
        y_true (list[np.ndarray]): The list of true ordered values.

    Returns:
        float: The average kendall tau correlation.
    """
    kendalls = []
    for a, b in zip(y_pred, y_true):
        k = kendall(a, b)
        # If the result of kendall is nan, then the arrays are empty or have 1 value.
        # This makes no sense, so we ignore it.
        if not np.isnan(k):
            kendalls.append(k)
    return float(np.mean(np.array(kendalls)))
