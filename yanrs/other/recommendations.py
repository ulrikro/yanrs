import numpy as np
import pandas as pd

from yanrs.preprocessing.rating_matrix import get_mapping_id


def get_recommendations_per_user(
    y_pred: np.ndarray, test: np.ndarray, top_n: int | None = None
):
    """Gets the top n recommendations for each user.

    Args:
        y_pred (np.ndarray): The rating matrix of filled in predictions.
        test (np.ndarray): The rating matrix of identical size to y_pred,
            where the entries for the predicted values of y_pred are defined,
            but the rest is NaN.
        top_n (int | None, optional): The amount of recommendations to get
            gets the top n with highest predicted score. Defaults to None,
            which means all documents which the ratings were predicted.

    Returns:
        _type_: _description_
    """
    recoms_per_user = []
    for u in range(y_pred.shape[0]):
        testvals = test[u, :]
        some = y_pred[u, ~np.isnan(testvals)].argsort()[::-1][:top_n]  # type: ignore
        recoms_per_user.append(some)
    return recoms_per_user


def get_recommended_titles(
    user_recommendations: np.ndarray,
    document_mapping: pd.Index,
    document_df: pd.DataFrame,
):
    pred_recommended_document_ids = [
        get_mapping_id(i, document_mapping) for i in user_recommendations
    ]
    return document_df[document_df["documentId"].isin(pred_recommended_document_ids)][
        "title"
    ].unique()
