# type: ignore
import matplotlib.pyplot as plt


def plot_evaluation_scores(
    evaluation_scores: list[float],
    ks: list[int],
    title: str,
    x_label: str,
    y_label: str,
    plot_for_presentation: bool = False,
):
    fig, ax = plt.subplots()
    if plot_for_presentation:
        fig.patch.set_alpha(0.0)
        ax.set_facecolor("none")
        ax.set_xlabel(x_label, fontsize=16, color="white")
        ax.set_ylabel(y_label, fontsize=16, color="white")
        ax.set_title(title, fontsize=18, color="white")
        ax.spines["bottom"].set_linewidth(2)
        ax.spines["bottom"].set_color("white")
        ax.spines["top"].set_linewidth(2)
        ax.spines["top"].set_color("white")
        ax.spines["right"].set_linewidth(2)
        ax.spines["right"].set_color("white")
        ax.spines["left"].set_linewidth(2)
        ax.spines["left"].set_color("white")
        ax.xaxis.labelpad = 10
        ax.yaxis.labelpad = 10
        ax.tick_params(axis="both", labelsize=14, color="white", labelcolor="white")
        ax.plot(ks, evaluation_scores, linewidth=3.0, color="white")
    else:
        ax.set_xlabel(x_label, fontsize=16)
        ax.set_ylabel(y_label, fontsize=16)
        ax.set_title(title, fontsize=18)
        ax.spines["bottom"].set_linewidth(2)
        ax.spines["top"].set_linewidth(2)
        ax.spines["right"].set_linewidth(2)
        ax.spines["left"].set_linewidth(2)
        ax.xaxis.labelpad = 10
        ax.yaxis.labelpad = 10
        ax.tick_params(axis="both", labelsize=14)
        ax.plot(ks, evaluation_scores, linewidth=3.0, color="black")
    plt.show()

def multi_plot_evaluation_scores(
    labels: list[str],
    evaluation_scores: list[list[float]],
    ks: list[float],
    title: str,
    x_label: str,
    y_label: str,
):
    fig, ax = plt.subplots()
    ax.set_xlabel(x_label, fontsize=16)
    ax.set_ylabel(y_label, fontsize=16)
    ax.set_title(title, fontsize=18)
    ax.spines["bottom"].set_linewidth(2)
    ax.spines["top"].set_linewidth(2)
    ax.spines["right"].set_linewidth(2)
    ax.spines["left"].set_linewidth(2)
    ax.tick_params(axis="both", labelsize=14)
    for evaluation_score, label in zip(evaluation_scores, labels):
        ax.plot(ks, evaluation_score, linewidth=3.0, label=label)
    # enable legend and increase font size
    ax.legend(fontsize=14)
    plt.tight_layout()
    plt.show()


if __name__ == "__main__":
    import numpy as np
    evaluation_scores = []
    for i in range(3):
        evaluation_score = np.random.rand(10) - i
        evaluation_scores.append(evaluation_score)
    ks = np.arange(10).tolist()
    multi_plot_evaluation_scores(["First Label", "Second Label", "Third Label"], evaluation_scores, ks,
                                 "title", "x_label", "y_label")