from typing import Callable, TypeVar

import numpy as np

from yanrs.metrics.types_metrics import Metric
from yanrs.recommenders.types_recommenders import Recommender

TRecommender = TypeVar("TRecommender", bound=Recommender)


def evaluate_model_per_hyperparams(
    model: TRecommender,
    train: np.ndarray,
    y_true: np.ndarray,
    eval_func: Metric,
    set_hyperparam: Callable[[TRecommender, int], None],
    hyperparam_values: list[int],
):
    """Perform hyperparameter optimization for the given training and test data."""
    evals = []
    model.fit(train)
    for i in hyperparam_values:
        set_hyperparam(model, i)
        predictions = model.predict_all()
        evals.append(eval_func(predictions, y_true))
    return evals
