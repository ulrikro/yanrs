import numpy as np
from sklearn.metrics.pairwise import cosine_similarity
from tqdm import trange


class ContentBasedRecommender:
    def __init__(self, embeddings: np.ndarray, kmost: int = 10) -> None:
        self.document_similarities = cosine_similarity(embeddings)
        self.kmost = kmost

    def fit(self, matrix_train: np.ndarray):
        self.rating_matrix = matrix_train

    def _predict_rating(self, user_document_similarities, user_ratings) -> float:
        k_most_similar_document_ratings = user_ratings[
            np.argpartition(user_document_similarities, -self.kmost, axis=1)[::, -self.kmost:]
        ]
        # Return average/median rating of the k most similar documents (ignoring NaNs)
        return np.nanmean(k_most_similar_document_ratings, axis=1)

    def predict_rating(self, user_index: int, document_index: int) -> float:
        return self._predict_rating(
            self.document_similarities.copy(),
            self.rating_matrix[user_index, :].copy()
        )[document_index]

    def predict_ratings_for_user(self, user_index: int) -> np.ndarray:
        user_ratings = self.rating_matrix[user_index, :]
        user_document_similarities = self.document_similarities.copy()
        user_document_similarities[:, np.isnan(user_ratings)] = -np.inf
        predicted = self._predict_rating(user_document_similarities, user_ratings)
        return predicted

    def predict_all(self) -> np.ndarray:
        ratings = np.ndarray(self.rating_matrix.shape)
        for u in trange(self.rating_matrix.shape[0]):
            ratings[u] = self.predict_ratings_for_user(u)
        return ratings
