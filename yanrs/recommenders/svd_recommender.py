import numpy as np


class SvdRecommender:
    def __init__(self, k: int = 2):
        self.k = k

    def fit(self, matrix_train: np.ndarray):
        # replace nan in train_ratings with mean of train_ratings
        matrix_train = np.nan_to_num(matrix_train, nan=np.nanmean(matrix_train).item())
        self.U, self.s, self.Vt = np.linalg.svd(matrix_train)

    def _U_reduced(self):
        return self.U[:, : self.k]

    def _s_reduced(self):
        return self.s[: self.k]

    def _Vt_reduced(self):
        return self.Vt[: self.k, :]

    def predict_rating(self, user_id: int, document_id: int) -> float:
        return float(
            self._U_reduced()[user_id, :]
            @ np.diag(self._s_reduced())
            @ self._Vt_reduced()[:, document_id]
        )

    def predict_ratings_for_user(self, user_id: int) -> np.ndarray:
        return (
            self._U_reduced()[user_id, :]
            @ np.diag(self._s_reduced())
            @ self._Vt_reduced()
        )

    def predict_all(self) -> np.ndarray:
        return self._U_reduced() @ np.diag(self._s_reduced()) @ self._Vt_reduced()
