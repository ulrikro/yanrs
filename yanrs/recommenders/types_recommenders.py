from typing import Protocol

import numpy as np


class Recommender(Protocol):
    def fit(self, matrix_train: np.ndarray):
        ...

    def predict_ratings_for_user(self, user_index: int) -> np.ndarray:
        ...

    def predict_all(self) -> np.ndarray:
        ...
