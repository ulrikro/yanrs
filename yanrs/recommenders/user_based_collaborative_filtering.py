from typing import cast

import numpy as np
from tqdm import tqdm

from yanrs.metrics.pearson import pearson


def create_pearson_matrix(rating_matrix: np.ndarray) -> np.ndarray:
    m_len = len(rating_matrix)
    p_matrix = np.zeros((m_len, m_len))
    for i in tqdm(range(m_len)):
        for j in range(m_len):
            p_matrix[i][j] = pearson(rating_matrix[i], rating_matrix[j])
    return p_matrix


class UserBasedCollaborativeFiltering:
    def __init__(self, kmost: int = 10) -> None:
        self.kmost = kmost

    def fit(self, matrix_train: np.ndarray):
        self.rating_matrix = matrix_train
        self.pearson_matrix = create_pearson_matrix(matrix_train)
        self.mean_rating = cast(float, np.nanmean(matrix_train))
        self.document_mean_ratings = np.nanmean(matrix_train, axis=0)
        self.document_mean_ratings[
            np.isnan(self.document_mean_ratings)
        ] = self.mean_rating
        self.user_mean_ratings = np.nanmean(matrix_train, axis=1)
        self.user_mean_ratings[np.isnan(self.user_mean_ratings)] = self.mean_rating

    def predict_rating(self, user_index: int, document_index: int) -> float:
        # Get all users that have rated the document
        all_users_that_rated_document = np.where(
            np.isfinite(self.rating_matrix[:, document_index])
        )[0]
        all_users_that_rated_document = all_users_that_rated_document[
            all_users_that_rated_document != user_index
        ]
        # Get the k most similar users to u
        most_similar_users_indices = all_users_that_rated_document[
            np.argsort(self.pearson_matrix[user_index, all_users_that_rated_document])[
                ::-1
            ][: self.kmost]
        ]
        # Predict the rating of the user
        user_ratings = self.rating_matrix[most_similar_users_indices, :]
        mean_rating_per_user = np.nanmean(user_ratings, axis=1)
        document_ratings = self.rating_matrix[
            most_similar_users_indices, document_index
        ]
        mean_centered_ratings = document_ratings - mean_rating_per_user
        top = np.sum(
            self.pearson_matrix[user_index, most_similar_users_indices]
            * mean_centered_ratings
        )
        bottom = np.sum(
            np.abs(self.pearson_matrix[user_index, most_similar_users_indices])
        )
        if bottom == 0.0 or np.isnan(bottom):
            return self.document_mean_ratings[document_index]

        return (top / bottom) + self.user_mean_ratings[user_index]

    def predict_ratings_for_user(self, user_index: int) -> np.ndarray:
        user_ratings = self.rating_matrix[user_index, :].copy()

        def apply(rating: float, document_index: int):
            if np.isnan(rating):
                return self.predict_rating(
                    user_index,
                    document_index,
                )
            return rating  # np.nan

        return np.array(
            [
                apply(rating, document_index)
                for document_index, rating in enumerate(user_ratings)
            ]
        )

    def predict_all(self) -> np.ndarray:
        return np.array(
            [
                self.predict_ratings_for_user(i)
                for i in tqdm(range(len(self.rating_matrix)))
            ]
        )
