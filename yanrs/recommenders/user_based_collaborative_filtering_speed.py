from typing import cast

import numpy as np
from tqdm import tqdm

from yanrs.metrics.pearson import pearson


def create_pearson_matrix(rating_matrix: np.ndarray) -> np.ndarray:
    m_len = len(rating_matrix)
    p_matrix = np.zeros((m_len, m_len))
    for i in tqdm(range(m_len)):
        for j in range(m_len):
            p_matrix[i][j] = pearson(rating_matrix[i], rating_matrix[j])
    return p_matrix


class UserBasedCollaborativeFiltering:
    def __init__(self, kmost: int = 15) -> None:
        self._kmost = kmost
        self.is_trained = False
        self.cached_predict_all: np.ndarray | None = None

    @property
    def kmost(self) -> int:
        return self._kmost

    @kmost.setter
    def kmost(self, kmost: int) -> None:
        if kmost < 1:
            raise ValueError("kmost must be greater than 0")
        self._kmost = kmost
        self.cached_predict_all = None

    def fit(self, matrix_train: np.ndarray):
        self.rating_matrix = matrix_train
        self.pearson_matrix = create_pearson_matrix(matrix_train)
        self.mean_rating = cast(float, np.nanmean(matrix_train))
        self.document_mean_ratings = np.nanmean(matrix_train, axis=0)
        self.document_mean_ratings[
            np.isnan(self.document_mean_ratings)
        ] = self.mean_rating
        self.user_mean_ratings = np.nanmean(matrix_train, axis=1)
        self.user_mean_ratings[np.isnan(self.user_mean_ratings)] = self.mean_rating
        self.centered_rating_matrix = (
            self.rating_matrix - self.user_mean_ratings[:, None]
        )
        self.is_trained = True

    def _predict_rating(self, document_index: int) -> float:
        # Get all users that have rated the document
        all_users_that_rated_document = np.where(
            np.isfinite(self.rating_matrix[:, document_index])
        )[0]
        if all_users_that_rated_document.shape[0] == 0:
            return self.document_mean_ratings[document_index]

        most_similar_users_indices = all_users_that_rated_document[
            np.argsort(self.pearson_matrix[:, all_users_that_rated_document])[:, ::-1][
                :, : self.kmost
            ]
        ]

        mean_centered_ratings = self.centered_rating_matrix[
            most_similar_users_indices, document_index
        ]

        matrix = np.zeros(most_similar_users_indices.shape)
        for i in range(self.pearson_matrix.shape[0]):
            matrix[i] = self.pearson_matrix[i, most_similar_users_indices[i]]

        # matrix2 = np.zeros(most_similar_users_indices.shape)
        # matrix2[np.arange(self.pearson_matrix.shape[0])[:, None], :most_similar_users_indices.
        # shape[1]] = \
        # self.pearson_matrix[np.arange(self.pearson_matrix.shape[0])[:, None],
        # most_similar_users_indices][:, np.newaxis, :]
        # assert np.allclose(matrix, matrix2)

        top = np.sum(matrix * mean_centered_ratings, axis=1)
        bottom = np.sum(np.abs(matrix), axis=1)

        no_similar_users = bottom == 0.0
        top[no_similar_users] = self.document_mean_ratings[document_index]
        bottom[no_similar_users] = 1.0
        centered_result = top / bottom
        centered_result[np.logical_not(no_similar_users)] += self.user_mean_ratings[
            np.logical_not(no_similar_users)
        ]
        return centered_result

    def predict_ratings_for_user(self, user_index: int) -> np.ndarray:
        return self.predict_all()[user_index]

    def predict_all(self) -> np.ndarray:
        if self.cached_predict_all is not None:
            return self.cached_predict_all
        result = np.zeros(self.rating_matrix.shape)
        for document_index in tqdm(range(self.rating_matrix.shape[1])):
            result[:, document_index] = self._predict_rating(document_index)
        result[np.isfinite(self.rating_matrix)] = self.rating_matrix[
            np.isfinite(self.rating_matrix)
        ]
        self.cached_predict_all = result
        return result
