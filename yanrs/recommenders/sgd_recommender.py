import numpy as np


class SgdRecommender:
    def __init__(
        self,
        k: int = 6,
        learning_rate: float = 0.00007,
        epochs: int = 20,
        regularization: float = 0.4,
        bias: bool = True,
    ):
        self.k = k
        self.learning_rate = learning_rate
        self.epochs = epochs
        self.regularization = regularization
        self.bias = bias

    def fit(self, matrix_train: np.ndarray):
        self.do_sdg(
            self.k,
            self.epochs,
            matrix_train,
            None,
            self.learning_rate,
            self.regularization,
            self.bias,
        )

    def fit_test(self, matrix_train: np.ndarray, matrix_test: np.ndarray):
        self.do_sdg(
            self.k,
            self.epochs,
            matrix_train,
            matrix_test,
            self.learning_rate,
            self.regularization,
            self.bias,
        )

    def do_sdg(
        self,
        k,
        epochs,
        train_ratings,
        test_ratings,
        learning_rate,
        regularization,
        bias,
    ):
        self.train_ratings = train_ratings
        self.P, self.Q, self.p, self.q = self.initiate_matrices_and_means(
            train_ratings, k
        )
        # get indeces of non-nan values
        indices_train = np.argwhere(~np.isnan(train_ratings))
        if test_ratings is not None:
            indices_test = np.argwhere(~np.isnan(test_ratings))
        else:
            indices_test = np.zeros(0)
        for i in range(epochs):
            train_error = 0.0
            np.random.shuffle(indices_train)
            for index in indices_train:
                train_error += self.do_stochastic_gradient_descent_step(
                    index[0], index[1], learning_rate, regularization, bias
                )
            train_error /= len(indices_train)
            test_error = 0.0
            if test_ratings is not None:
                for index in indices_test:
                    test_error += (
                        self.calc_diff(
                            index[0], index[1], test_ratings[index[0], index[1]], bias
                        )
                        ** 2
                    )
                test_error /= len(indices_test)
            print(f"train error: {train_error}, test error: {test_error}")

    def predict_rating(self, user_id: int, document_id: int) -> float:
        return (
            self.p[user_id]
            + self.q[document_id]
            + np.dot(self.P[user_id, :], self.Q[document_id, :])
        )

    def predict_ratings_for_user(self, user_index: int) -> np.ndarray:
        return self.p[user_index] + self.q + np.dot(self.P[user_index, :], self.Q.T)

    def predict_all(self) -> np.ndarray:
        return self.p[:, None] + self.q[None, :] + np.dot(self.P, self.Q.T)

    def initiate_matrices_and_means(self, train_ratings, k):
        overall_mean = np.nanmean(train_ratings).item()
        p = np.nan_to_num(np.nanmean(train_ratings, axis=1), nan=overall_mean) / 2
        q = np.nan_to_num(np.nanmean(train_ratings, axis=0), nan=overall_mean) / 2
        # variance of users and items of train_ratings when removing mean
        # p_var = np.nan_to_num(np.nanvar(train_ratings - p[:, None] * 2, axis=1), nan=1)
        # q_var = np.nan_to_num(np.nanvar(train_ratings - q[None, :] * 2, axis=0), nan=1)
        P = np.random.rand(train_ratings.shape[0], k)  # * np.sqrt(overall_var / k)
        Q = np.random.rand(train_ratings.shape[1], k)  # * np.sqrt(overall_var / k)

        return P, Q, p, q

    def do_stochastic_gradient_descent_step(
        self, user, item, learning_rate, reg=0, bias=False
    ):
        r_u_i = self.train_ratings[user, item]
        div = self.calc_diff(user, item, r_u_i, bias)
        if bias:
            self.p[user] = self.p[user] - learning_rate * (div + reg * self.p[user])
            self.q[item] = self.q[item] - learning_rate * (div + reg * self.q[item])

        self.P[user, :] = self.P[user, :] - learning_rate * (
            div * self.Q[item, :] + reg * self.P[user, :]
        )
        self.Q[item, :] = self.Q[item, :] - learning_rate * (
            div * self.P[user, :] + reg * self.Q[item, :]
        )
        return div * div

    def calc_diff(self, user, item, r_u_i, bias):
        if bias:
            return (
                self.p[user] + self.q[item] + self.P[user, :] @ self.Q[item, :]
            ) - r_u_i
        else:
            return self.P[user, :] @ self.Q[item, :] - r_u_i
