import numpy as np
import pandas as pd


def create_reading_time_matrix_and_word_count_vector(
    table: pd.DataFrame,
) -> tuple[pd.DataFrame, pd.DataFrame]:
    """Creates a n x m matrix from the input table where n is the number of users
    and m is the number of items, and the values are the reading time of the
    user per article.

    Args:
        logs (pd.DataFrame): A DataFrame with at least
            the following columns: userId, documentId, activeTime.

    Returns:
        pd.DataFrame: The reading time matrix as a DataFrame.
    """
    matrix = table.pivot_table(
        index="userId", columns="documentId", values="activeTime", aggfunc="sum"
    )  # .fillna(0)
    matrix = matrix.reindex(sorted(matrix.columns), axis=1)
    word_counts_out = (
        table.groupby("documentId")
        .agg(body_word_count=("body_word_count", "first"))
        .sort_values(by=["documentId"])
        .reset_index()
        .drop(columns=["documentId"])
    )
    return matrix, word_counts_out


def get_mapping_index(id: str, mapping: pd.Index) -> int:
    return np.where(mapping == id)[0][0]


def get_mapping_id(index: int, mapping: pd.Index) -> str:
    return str(mapping[index])


def create_reading_time_matrix_numpy(
    logs_matrix: pd.DataFrame,
    max_reading_time: int = 15 * 60,
) -> tuple[np.ndarray, pd.Index, pd.Index, np.ndarray]:
    """Creates a n x m matrix from the input table where n is the number of users
    and m is the number of items, and the values are the reading time of the
    user per article. Also returns two mapping functions to get the indexes
    of users and documents from their ids respectively.

    Args:
        logs_matrix (pd.DataFrame): A DataFrame with at least
            the following columns: userId, documentId, activeTime.
        word_count_vector (pd.DataFrame): A DataFrame containing the word count for each document.
        max_reading_time (int, optional): The cutoff for the reading time. Defaults to 15 * 60.

    Returns:
        tuple[np.ndarray, pd.Index, pd.Index]: The tuple containing
            - the reading time matrix as a two-dimensional numpy array.
            - mapping array of user ids, where the index is their index in the
                matrix (first dimension).
            - mapping array of document ids, where the index is their index in the
                matrix (second dimension).
            - word count vector as a one-dimensional numpy array.

    Example usage:
        # For some fulfilling dataframe, df.
        m, user_mapping, document_mapping = create_reading_time_matrix_numpy(df)

        # Using helper function get_mapping_index to get the index of a user/document id
        value = m[get_mapping_index("ex_user_id", user_mapping)] \
                [get_mapping_index("ex_document_id", document_mapping)]

        # Using helper function get_mapping_id to get the id of a user from the index.
        user_id_at_index_5 = get_mapping_id(5, user_mapping)
        document_id_at_index_10 = get_mapping_id(10, document_mapping)


    """
    (
        reading_time_matrix,
        word_count_vector_out,
    ) = create_reading_time_matrix_and_word_count_vector(logs_matrix)
    reading_time_matrix[reading_time_matrix > max_reading_time] = max_reading_time
    return (
        reading_time_matrix.to_numpy(),
        reading_time_matrix.index,
        reading_time_matrix.columns,
        word_count_vector_out.to_numpy()[::, 0],
    )


def create_rating_matrix(
    reading_time_matrix: np.ndarray, word_counts: np.ndarray, max_rating: float
) -> np.ndarray:
    # reading_time_matrix is a user_id x document_id matrix
    # word_counts is a document_id vector
    # Normalize the reading time by the average reading time for that article
    new_matrix = reading_time_matrix.copy()
    new_matrix = new_matrix / word_counts[np.newaxis, :]
    new_matrix[new_matrix > max_rating] = max_rating
    return new_matrix


def create_ratings_from_df(
    df: pd.DataFrame, max_rating=1.0
) -> tuple[np.ndarray, pd.Index, pd.Index]:
    (
        m,
        user_mapping,
        document_mapping,
        word_counts_np,
    ) = create_reading_time_matrix_numpy(df)
    rating_matrix = create_rating_matrix(m, word_counts_np, max_rating=max_rating)
    return rating_matrix, user_mapping, document_mapping
