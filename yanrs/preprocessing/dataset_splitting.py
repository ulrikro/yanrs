import numpy as np
from tqdm import tqdm


def custom_train_test_split(
    original_matrix: np.ndarray, relativ_testset_size: float
) -> tuple[np.ndarray, np.ndarray]:
    """Creates a testset and trainingset from the original matrix. All testvalues are set to NaN in
    the trainingset.

    Args:
        original_matrix (np.ndarray): The original matrix.
        relativ_testset_size (float): The size of the testset as relative size of the original
        matrix.

    Returns:
        tuple[np.ndarray, np.ndarray]: The tuple containing
            - the testset matrix as a two-dimensional numpy array.
            - the trainingset matrix as a two-dimensional numpy array.
    """
    # create testset with same shape as original matrix but with all values set to NaN
    testset = np.full(original_matrix.shape, np.nan)
    trainingset = original_matrix.copy()
    for user in tqdm(range(original_matrix.shape[0])):
        indices_of_non_nan = np.where(~np.isnan(original_matrix[user, :]))[0]
        testset_size_for_user = int(relativ_testset_size * indices_of_non_nan.shape[0])
        testset_items = np.random.choice(
            indices_of_non_nan,  # original_matrix[user, :].nonzero()[0],
            size=testset_size_for_user,
            replace=False,
        )
        trainingset[user, testset_items] = np.nan
        testset[user, testset_items] = original_matrix[user, testset_items]
    return trainingset, testset
