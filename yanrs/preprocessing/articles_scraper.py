import re
import time
from concurrent.futures import ThreadPoolExecutor

import pandas as pd
import requests  # type: ignore

from yanrs.preprocessing.feather_log_loader import concat_all_feather_files_in_folder


def scrape_article(url):
    try:
        r = requests.get(url)
    except ConnectionError:
        print(url, "CONNECTION ERROR")
        return None
    if r.status_code != 200:
        if r.status_code == 404:
            print(url, "NOT FOUND")
        else:
            print(url, "ERROR")
        return None
    m = re.search("schibsted:{(.*),fetchTagAction:", r.text)
    if m is None:
        print(url, "MALFORMED")
        return None
    text_parts = re.findall('value:"(.*?)"', m[1])
    if len(text_parts) <= 0:
        print(url, "EMPTY")
        return None
    return "\n".join(text_parts)


def scrape_article_texts_futures(urls):
    texts = []
    with ThreadPoolExecutor(max_workers=8) as executor:
        for response in list(executor.map(scrape_article, urls)):
            texts.append(response)
            print(response)
    print(texts)
    return texts


def scrape_article_texts(urls):
    article_texts = []
    searched, valid, ok, bad_response, missing_content, pattern_failed = (
        0,
        0,
        0,
        0,
        0,
        0,
    )
    total = len(urls)
    for url in urls:
        searched += 1
        article_text = None
        print(url)
        r = requests.get(url)
        if r.status_code == 200:
            ok += 1
            article = r.text
            m = re.search("schibsted:{(.*),fetchTagAction:", article)
            if m is not None:
                article_obj = f"{{{m[1]}}}"
                text_parts = re.findall('value:"(.*?)"', article_obj)
                if len(text_parts) > 0:
                    article_text = "\n".join(text_parts)
                    print("OK")
                else:
                    missing_content += 1
                    print("MISSING CONTENT!")
            else:
                pattern_failed += 1
                print("PATTERN FAILED!")
        else:
            bad_response += 1
            print("BAD RESPONSE!")
        if article_text is not None:
            valid += 1
        print(
            f"{valid}/{ok}({bad_response}/{missing_content}/{pattern_failed})/{searched}/{total}"
            + f"({int(100 * valid / searched)}% / {int(100 * searched / total)}%)"
        )
        article_texts.append(article_text)
    return article_texts


def main():
    start = time.time()
    df = (
        concat_all_feather_files_in_folder("active1000")
        .dropna(subset=["url", "documentId"])
        .drop_duplicates(subset=["documentId"])[:1]
    )
    print(f"Found {df.index.size} probable article urls")
    # df["text"] = scrape_article_texts(df["url"])
    df["text"] = scrape_article_texts_futures(df["url"])
    df_text = df[["documentId", "text"]].dropna(subset=["text"]).reset_index(drop=True)
    print(df_text.head())
    print(df_text.index.size)
    df_text.to_feather("scrapeyard.feather")
    df_from_feather = pd.read_feather("scrapeyard.feather")
    print(df_from_feather.head())
    print(df_from_feather.index.size)
    print(time.time() - start)


if __name__ == "__main__":
    main()
