import json
import os

import pandas as pd


def concat_all_feather_files_in_folder(folder: str) -> pd.DataFrame:
    # print all files in folder
    df = pd.DataFrame()
    for filename in os.listdir(folder):
        if filename.endswith(".feather"):
            df = pd.concat(
                [df, pd.read_feather(os.path.join(folder, filename))],
                ignore_index=True,
            )
    # df = df.dropna(subset=["activeTime", "userId", "documentId"])
    # .reset_index(drop=True)
    return df


def read_adressa_file(file) -> pd.DataFrame:
    with open(file, "r") as f:
        content = f.read()
    parsed_values = []
    decoder = json.JSONDecoder()
    while content:
        value, new_start = decoder.raw_decode(content)
        content = content[new_start:].strip()
        # parse dates like '2017-03-17T17:39:35.000Z'
        value["publishtime"] = pd.to_datetime(
            value["publishtime"], format="%Y-%m-%dT%H:%M:%S.%fZ"
        )
        # parse column time from unixseconds to datetime
        value["time"] = pd.to_datetime(value["time"], unit="s")
        # You can handle the value directly in this loop:
        parsed_values.append(value)

    return pd.DataFrame(parsed_values)


def get_body_word_count(articles: pd.DataFrame, min_word_count=10) -> pd.DataFrame:
    articles["body"] = articles["body"].apply(
        lambda x: " ".join(x) if x is not None else ""
    )
    articles["body_word_count"] = articles["body"].apply(lambda x: len(x.split()))
    articles = articles[articles["body_word_count"] >= min_word_count]
    return articles


def drop_lines_without_existing_article(
    logs: pd.DataFrame, articles: pd.DataFrame
) -> tuple[pd.DataFrame, pd.DataFrame]:
    article_ids = set(articles["id"].unique())
    document_ids = set(logs["documentId"].unique())
    # filter out all logs that have a documentId that is not in the set of article.id's
    filtered_logs = logs[logs["documentId"].isin(article_ids)]
    filtered_articles = articles[articles["id"].isin(document_ids)]
    # sort filtered_logs and filtered_articles by documentId and id
    filtered_logs = filtered_logs.sort_values(by=["documentId"])
    filtered_articles = filtered_articles.sort_values(by=["id"])
    filtered_articles = filtered_articles.reset_index(drop=True)
    filtered_logs = filtered_logs.reset_index(drop=True)
    return filtered_logs, filtered_articles


def main():
    # Documenting how data/active1000_filtered.feater is created
    df = pd.read_feather("data/active1000.feather")
    df = df.dropna(subset=["activeTime", "userId", "documentId"]).reset_index(drop=True)
    articles = pd.read_feather("data/articles.feather")
    articles = articles.dropna(subset=["body"]).reset_index(drop=True)
    filtered_articles_word_count = get_body_word_count(articles, min_word_count=10)
    filtered_logs, filtered_articles = drop_lines_without_existing_article(
        df, filtered_articles_word_count
    )

    # join filtered_logs and filtered_articles on documentId and id without adding id to
    # filtered_logs
    filtered_logs = filtered_logs.merge(
        filtered_articles[["id", "body_word_count"]],
        left_on="documentId",
        right_on="id",
        how="inner",
    )
    # remove id from filtered_logs
    filtered_logs = filtered_logs.drop(columns=["id"])

    # write to feather file

    filtered_articles.to_feather("data/articles_filtered.feather")
    filtered_logs.to_feather("data/active1000_filtered.feather")
    filtered_articles[["id", "body_word_count"]].to_feather(
        "data/word_counts_filtered.feather"
    )
    #    df.to_feather("../../data/active1000_filtered.feather")
    print(df)


if __name__ == "__main__":
    main()
