import json
import os
from pathlib import Path

import pandas as pd
from tqdm import tqdm


def read_articles_dir(dir_path: str) -> pd.DataFrame:
    return pd.concat(
        [read_article(os.path.join(dir_path, f)) for f in tqdm(os.listdir(dir_path))],
        ignore_index=True,
    )


def read_article(path: str) -> pd.DataFrame:
    with open(path, "r") as f:
        data = json.load(f)
    fields_df = (
        pd.json_normalize(data["fields"])[["field", "value"]]  # type: ignore
        .set_index("field")
        .T.reset_index(drop=True)
    )
    data.pop("fields")
    return fields_df.merge(pd.DataFrame(data, index=[0]))


def force_dataframe_columns_to_list_type(
    df: pd.DataFrame, columns: list[str]
) -> pd.DataFrame:
    dfc = df.copy()
    for c in columns:
        dfc[c] = dfc[c].map(lambda a: [a] if isinstance(a, str) else a)
    return dfc


def main():
    df = read_articles_dir("articles")
    df = force_dataframe_columns_to_list_type(
        df,
        [
            # List of columns mixing list and non-list values
            "author",
            "body",
            "heading",
            "kw-category",
            "kw-entity",
            "kw-location",
            "kw-person",
            "kw-taxonomy",
            "taxonomy",
            "kw-company",
            "kw-classification",
            "kw-concept",
            "adressa-tag",
            "keyword",
            "og-image",
            "og-site-name",
            "og-title",
            "og-type",
            "og-url",
        ],
    )
    feather_path = Path("data") / "articles.feather"
    df.to_feather(feather_path)
    print(pd.read_feather(feather_path))


if __name__ == "__main__":
    main()
