import numpy as np
import pandas as pd
import spacy
from tqdm import tqdm

"""
python -m spacy download nb_core_news_md
"""

# Load the Norwegian language model
nlp = spacy.load("nb_core_news_md")


def w2v_embeddings(article_texts: list[str]) -> np.ndarray:
    return np.array([nlp(text).vector for text in tqdm(article_texts)])


def main():
    articles = pd.read_feather("data/articles_filtered.feather")
    articles_texts = articles["body"].values.tolist()
    embeddings = w2v_embeddings(articles_texts)
    np.save("data/w2v_embeddings.npy", embeddings)


if __name__ == "__main__":
    main()
