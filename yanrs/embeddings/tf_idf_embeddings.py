from typing import List

import numpy as np
import pandas as pd
import spacy as spacy
from sklearn.decomposition import TruncatedSVD  # type: ignore
from sklearn.feature_extraction.text import TfidfVectorizer  # type: ignore


class TfIdfPca:
    # run this first: python -m spacy download nb_core_news_md
    def __init__(self, k: int = 100, min_df: int = 2, max_df: float = 0.5):
        self._k = k
        self._tfidfVectorizer = TfidfVectorizer(min_df=min_df, max_df=max_df)
        self._truncated_svd = TruncatedSVD(n_components=k)
        self._nlp = spacy.load(
            "nb_core_news_md",
            disable=[
                "parser",
                "ner",
                "tagger",
                "textcat",
                "entity_ruler",
                "merge_noun_chunks",
                "merge_entities",
                "merge_subtokens",
            ],
        )

    def fit(self, documents: List[str]) -> np.ndarray:
        documents = self._apply_lemmatization(documents)
        print("Done lemmatization")
        tfidf = self._tfidfVectorizer.fit_transform(documents)
        print("Done tfidf")
        return self._truncated_svd.fit_transform(tfidf)

    def transform(self, documents: List[str]) -> np.ndarray:
        documents = self._apply_lemmatization(documents)
        tfidf = self._tfidfVectorizer.transform(documents)
        return self._truncated_svd.transform(tfidf)

    def _apply_lemmatization(self, documents: List[str]) -> List[str]:
        return_value = []
        for document in documents:
            return_value.append(
                " ".join([token.lemma_ for token in self._nlp(document)])
            )
        return return_value


def get_tfidf_embeddings_for_texts(
    articles: list[str], k: int = 100, min_df: int = 10, max_df: float = 0.5
) -> np.ndarray:
    tfidf_pca = TfIdfPca(k, min_df, max_df)
    return tfidf_pca.fit(articles)


if __name__ == "__main__":
    df = pd.read_feather("data/articles_filtered.feather")
    embeddings = get_tfidf_embeddings_for_texts(df["body"][0:200])
    # save sol (of type np.ndarray) to disk sol.npy
    np.save("data/tfidf_embedding_articles_filtered.npy", embeddings)
